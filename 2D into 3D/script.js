var theMan = {
    rangeX: 15,
    rangeY: 7,
}

var theGirl = {
    rangeX: 30,
    rangeY: 15,
}

$(document).ready(function() {
    $("body").on("mousemove", function(e) {
        var x = e.pageX;
        var y = e.pageY;
        var posX = theMan.rangeX * (x / $(window).width());
        var posY = theMan.rangeY * (y / $(window).height());
        $("#c-section__the-man").css("transform", `translate(-${posX}px, -${posY}px)`);

        var posX = theGirl.rangeX * (x / $(window).width());
        var posY = theGirl.rangeY * (y / $(window).height());
        $("#c-section__the-girl").css("transform", `translate(-${posX}px, -${posY}px)`);
    })
})